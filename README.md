Unidade de Materiais Elétricos - Soprano

Materiais elétricos de confiança para uso industrial e residencial. Minidisjuntores, DPS, DR, interruptores, quadros, plugues e sistema fotovoltaico.

Com mais de 65 anos de história, buscamos desenvolver produtos que façam parte da vida das pessoas, surpreendendo e inovando em cada detalhe. Comercializamos uma ampla linha de produtos através de cinco unidades de negócios: Fechaduras e Ferragens, Materiais Elétricos, Utilidades Térmicas, Componentes para Móveis e México.



São milhares de itens produzidos para atender os mercados de construção civil, materiais elétricos, moveleiro e utilidades domésticas.


Unidade de Materiais Elétricos - Soprano

RSC 453, 5542 - Desvio Rizzo
CEP: 95.110-310, Caxias do Sul - RS
Telefone: (54) 2101-7070
Email: eletrica@soprano.com.br
https://www.soprano.com.br/produtos/materiais-eletricos